
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

INSERT_DATA = 0         # (key, value) as (str, str)
REMOVE_DATA = 1         # key as str
DATA_CHANGED = 2        # is_empty as bool
APPLY_CHANGES = 3       # None
