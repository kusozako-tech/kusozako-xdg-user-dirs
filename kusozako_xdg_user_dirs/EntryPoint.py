
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.Transmitter import FoxtrotTransmitter
from .model.Model import DeltaModel
from .box.Box import DeltaBox


class DeltaEntryPoint(DeltaEntity):

    def _delta_call_model_signal(self, user_data):
        self._transmitter.transmit(user_data)

    def _delta_call_register_model_object(self, object_=None):
        self._transmitter.register_listener(object_)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = FoxtrotTransmitter()
        DeltaModel(self)
        DeltaBox(self)
