
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import Gtk
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from libkusozako3 import HomeDirectory
from libkusozako3.file_chooser.FileChooser import DeltaFileChooser
from kusozako_xdg_user_dirs import ModelSignals

MODEL = {
    "type": "select-directory",
    "id": None,
    "title": _("Select Directory"),
    "directory": None,
    "read-write-only": True
    }


class DeltaEntity(Gtk.Entry, DeltaEntity):

    @classmethod
    def new_for_model(cls, parent, model):
        entry = cls(parent)
        entry.set_model(model)

    def _change_directory(self, path):
        if path == self._original:
            param = ModelSignals.REMOVE_DATA, self._directory
        else:
            param = ModelSignals.INSERT_DATA, (self._directory, path)
        self._raise("delta > model signal", param)

    def _on_icon_press(self, entry, position, event):
        path = DeltaFileChooser.run_for_model(self, MODEL)
        if path is not None:
            self._change_directory(path)
            self.set_text(HomeDirectory.shorten(path))

    def set_model(self, model):
        self._directory = model["directory"]
        self._original = GLib.get_user_special_dir(model["directory"])
        self.set_text(HomeDirectory.shorten(self._original))
        gio_file = Gio.File.new_for_path(self._original)
        file_info = gio_file.query_info("*", 0)
        gio_icon = file_info.get_icon()
        self.set_icon_from_gicon(Gtk.EntryIconPosition.SECONDARY, gio_icon)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Entry.__init__(self, margin=Unit(1), editable=False)
        self.connect("icon-press", self._on_icon_press)
        user_data = self, (1, 0, 2, 1)
        self._raise("delta > attach to grid", user_data)
