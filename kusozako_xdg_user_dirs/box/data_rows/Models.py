
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib

MODELS = (
    {
        "label": _("DESKTOP"),
        "css": "primary-surface-color-class",
        "directory": GLib.UserDirectory.DIRECTORY_DESKTOP
    },
    {
        "label": _("DOCUMENTS"),
        "css": "secondary-surface-color-class",
        "directory": GLib.UserDirectory.DIRECTORY_DOCUMENTS
    },
    {
        "label": _("DOWNLOAD"),
        "css": "primary-surface-color-class",
        "directory": GLib.UserDirectory.DIRECTORY_DOWNLOAD
    },
    {
        "label": _("MUSIC"),
        "css": "secondary-surface-color-class",
        "directory": GLib.UserDirectory.DIRECTORY_MUSIC
    },
    {
        "label": _("PICTURES"),
        "css": "primary-surface-color-class",
        "directory": GLib.UserDirectory.DIRECTORY_PICTURES
    },
    {
        "label": _("PUBLIC SHARE"),
        "css": "secondary-surface-color-class",
        "directory": GLib.UserDirectory.DIRECTORY_PUBLIC_SHARE
    },
    {
        "label": _("TEMPLATES"),
        "css": "primary-surface-color-class",
        "directory": GLib.UserDirectory.DIRECTORY_TEMPLATES
    },
    {
        "label": _("VIDEOS"),
        "css": "secondary-surface-color-class",
        "directory": GLib.UserDirectory.DIRECTORY_VIDEOS
    },
)
