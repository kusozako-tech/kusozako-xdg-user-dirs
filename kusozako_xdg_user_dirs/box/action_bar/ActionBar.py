
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from .CancelButton import DeltaCancelButton
from .ApplyButton import DeltaApplyButton


class DeltaActionBar(Gtk.Box, DeltaEntity):

    def _delta_call_pack_start(self, widget):
        self.pack_start(widget, False, False, 0)

    def _delta_call_pack_end(self, widget):
        self.pack_end(widget, False, False, 0)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        self.set_size_request(-1, Unit(6))
        DeltaCancelButton(self)
        DeltaApplyButton(self)
        self._raise("delta > css", (self, "primary-surface-color-class"))
        self._raise("delta > add to container", self)
