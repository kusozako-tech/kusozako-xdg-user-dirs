
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit


class DeltaSpacer(Gtk.Box, DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, vexpand=True)
        self.set_size_request(-1, Unit(3))
        self._raise("delta > add to container", self)
