
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_xdg_user_dirs import ModelSignals
from .Action import AlfaAction


class DeltaRemoveData(AlfaAction):

    SIGNAL = ModelSignals.REMOVE_DATA

    def _action(self, key=None):
        changes = self._enquiry("delta > changes")
        del changes[key]
        param = ModelSignals.DATA_CHANGED, len(changes) == 0
        self._raise("delta > model signal", param)
