
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .InsertData import DeltaInsertData
from .RemoveData import DeltaRemoveData
from .ApplyChanges import DeltaApplyChanges


class EchoActions:

    def __init__(self, parent):
        DeltaInsertData(parent)
        DeltaRemoveData(parent)
        DeltaApplyChanges(parent)
