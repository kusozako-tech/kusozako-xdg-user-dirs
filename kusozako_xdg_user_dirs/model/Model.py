
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .actions.Actions import EchoActions


class DeltaModel(DeltaEntity):

    def _delta_info_changes(self):
        return self._changes

    def __init__(self, parent):
        self._parent = parent
        self._changes = {}
        EchoActions(self)
