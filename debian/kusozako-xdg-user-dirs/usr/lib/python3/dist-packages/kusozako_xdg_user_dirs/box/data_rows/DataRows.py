
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .data_row.DataRow import DeltaDataRow
from .Models import MODELS


class EchoDataRows:

    def __init__(self, parent):
        for model in MODELS:
            DeltaDataRow.new_for_model(parent, model)
