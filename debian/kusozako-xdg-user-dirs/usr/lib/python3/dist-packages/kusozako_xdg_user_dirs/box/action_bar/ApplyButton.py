
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from kusozako_xdg_user_dirs import ModelSignals


class DeltaApplyButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        param = ModelSignals.APPLY_CHANGES, None
        self._raise("delta > model signal", param)

    def receive_transmission(self, user_data):
        signal, is_empty = user_data
        if signal == ModelSignals.DATA_CHANGED:
            self.set_sensitive(not is_empty)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            _("Apply"),
            relief=Gtk.ReliefStyle.NONE,
            margin=Unit(0.5),
            sensitive=False
            )
        self.set_size_request(-1, Unit(6))
        self.connect("clicked", self._on_clicked)
        self._raise("delta > pack end", self)
        self._raise("delta > register model object", self)
