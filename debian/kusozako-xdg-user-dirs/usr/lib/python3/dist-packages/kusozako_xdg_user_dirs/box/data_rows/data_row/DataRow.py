
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from .Entry import DeltaEntity


class DeltaDataRow(Gtk.Grid, DeltaEntity):

    @classmethod
    def new_for_model(cls, parent, model):
        data_row = cls(parent)
        data_row.set_model(model)

    def _delta_call_attach_to_grid(self, user_data):
        widget, geometry = user_data
        self.attach(widget, *geometry)

    def set_model(self, model):
        label = Gtk.Label(model["label"], xalign=1, yalign=0.5, margin=Unit(1))
        self.attach(label, 0, 0, 1, 1)
        DeltaEntity.new_for_model(self, model)
        self._raise("delta > css", (self, model["css"]))

    def __init__(self, parent):
        self._parent = parent
        Gtk.Grid.__init__(self,
            column_homogeneous=True,
            column_spacing=Unit(1)
            )
        self.set_size_request(-1, Unit(6))
        self._raise("delta > add to container", self)
