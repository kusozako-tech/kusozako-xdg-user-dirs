
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_xdg_user_dirs import ModelSignals
from .Action import AlfaAction


class DeltaInsertData(AlfaAction):

    SIGNAL = ModelSignals.INSERT_DATA

    def _action(self, param=None):
        key, path = param
        changes = self._enquiry("delta > changes")
        changes[key] = path
        param = ModelSignals.DATA_CHANGED, len(changes) == 0
        self._raise("delta > model signal", param)
