
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from libkusozako3 import HomeDirectory
from libkusozako3.message_dialog.MessageDialog import DeltaMessageDialog
from kusozako_xdg_user_dirs import ModelSignals
from .Action import AlfaAction

GLIB_INDEX = {
    GLib.UserDirectory.DIRECTORY_DESKTOP :"DESKTOP",
    GLib.UserDirectory.DIRECTORY_DOCUMENTS :"DOCUMENTS",
    GLib.UserDirectory.DIRECTORY_DOWNLOAD :"DOWNLOAD",
    GLib.UserDirectory.DIRECTORY_MUSIC :"MUSIC",
    GLib.UserDirectory.DIRECTORY_PICTURES :"PICTURES",
    GLib.UserDirectory.DIRECTORY_PUBLIC_SHARE :"PUBLICSHARE",
    GLib.UserDirectory.DIRECTORY_TEMPLATES :"TEMPLATES",
    GLib.UserDirectory.DIRECTORY_VIDEOS :"VIDEOS"
    }
DIALOG_MODEL = {
    "default-response": 0,
    "icon-name": "emblem-ok-symbolic",
    "buttons": (_("Close Application"),)
    }


class DeltaApplyChanges(AlfaAction):

    SIGNAL = ModelSignals.APPLY_CHANGES

    def _update(self, key, path):
        directory = GLIB_INDEX[key]
        command = ["xdg-user-dirs-update", "--set", directory, path]
        subprocess = Gio.Subprocess.new(command, Gio.SubprocessFlags.NONE)
        subprocess.wait(None)
        return _("{} directory changed to {}\n").format(
            directory,
            HomeDirectory.shorten(path)
            )

    def _action(self, param=None):
        message = ""
        changes = self._enquiry("delta > changes")
        for key, path in changes.items():
            message += self._update(key, path)
        model = DIALOG_MODEL.copy()
        model["message"] = message
        _ = DeltaMessageDialog.run_for_model(self, model)
        self._raise("delta > application force quit")
